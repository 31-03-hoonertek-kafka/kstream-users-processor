package com.classpath.streamusers.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.SerializationException;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class JsonSerializer<User> implements Serializer<User> {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        //todo
    }

    @Override
    public byte[] serialize(String topic, User user) {
        if(user == null){
            System.out.println("User cannot be null");
            return null;
        }
        System.out.println("Now serializing user into bytes");
        try {
            return objectMapper.writeValueAsBytes(user);
        } catch (JsonProcessingException e) {
            throw new SerializationException("Exception while serializing the user object");
        }
    }
    @Override
    public void close() {
        //todo
    }
}
