package com.classpath.streamusers.serializer;

import com.classpath.streamusers.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.SerializationException;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.util.Map;

public class JsonDeserializer implements Deserializer<User> {

    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        //todo
    }

    @Override
    public User deserialize(String topic, byte[] data) {
        if(data ==null){
            System.out.println("Null data received");
            return null;
        }
        try {
            User user = objectMapper.readValue(data, User.class);
            return user;
        } catch (IOException e) {
            e.printStackTrace();
            throw new SerializationException("Exception while deserializing the bytes to user object "+e.getMessage());
        }
    }

    @Override
    public void close() {
        //todo
    }
}
