package com.classpath.streamusers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamUsersApplication {

    public static void main(String[] args) {
        SpringApplication.run(StreamUsersApplication.class, args);
    }

}
