package com.classpath.streamusers.serdes;

import com.classpath.streamusers.model.User;
import com.classpath.streamusers.serializer.JsonDeserializer;
import com.classpath.streamusers.serializer.JsonSerializer;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import org.springframework.boot.jackson.JsonObjectSerializer;

import java.util.Map;

public class UserSerde implements Serde<User>  {
    private JsonSerializer jsonSerializer = new JsonSerializer();
    private JsonDeserializer jsonDeserializer = new JsonDeserializer();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        //todo
    }

    @Override
    public void close() {
        //todo
    }

    @Override
    public Serializer<User> serializer() {
        return jsonSerializer;
    }

    @Override
    public Deserializer<User> deserializer() {
        return jsonDeserializer;
    }
}
