package com.classpath.streamusers.serdes;

import com.classpath.streamusers.model.User;
import org.apache.kafka.common.serialization.Serde;

public class StreamsSerdes {

    public static Serde<User> UserSerde(){
        return new UserSerde();
    }
}
