package com.classpath.streamusers.config;

public class AppConfig {
    public final static String APPLICATION_ID="streaming-app";
    public final static String BOOTSTRAP_SERVERS="139.59.64.198:9092,157.245.98.120:9092,128.199.16.50:9092";
    public final static String TOPIC_NAME="new-users-topic";
    public final static int NUMBER_OF_ITERATIONS=1000;
}
