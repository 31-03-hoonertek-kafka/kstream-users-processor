package com.classpath.streamusers.producer;

import com.classpath.streamusers.config.AppConfig;
import com.classpath.streamusers.model.User;
import com.classpath.streamusers.serializer.JsonDeserializer;
import com.classpath.streamusers.serializer.JsonSerializer;
import com.github.javafaker.Faker;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

import static com.classpath.streamusers.config.AppConfig.*;

public class UserDatProducer {
    public static void generateTestData(){
        Properties properties = new Properties();
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, APPLICATION_ID);
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        KafkaProducer<String, User> producer = new KafkaProducer<>(properties);
        Faker faker = new Faker();
        for (int index = 0; index < NUMBER_OF_ITERATIONS; index++) {
            User user = User.builder()
                            .age(faker.number().numberBetween(10, 50))
                            .name(faker.name().firstName())
                            .id(faker.number().numberBetween(20, 40000))
                            .build();
            producer.send(new ProducerRecord<>(TOPIC_NAME, user.getId()+ "-"+user.getName(), user));
        }
    }
}
