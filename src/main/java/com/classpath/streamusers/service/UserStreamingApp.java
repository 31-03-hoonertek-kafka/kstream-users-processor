package com.classpath.streamusers.service;

import com.classpath.streamusers.config.AppConfig;
import com.classpath.streamusers.model.User;
import com.classpath.streamusers.producer.UserDatProducer;
import com.classpath.streamusers.serdes.StreamsSerdes;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Properties;

import static com.classpath.streamusers.config.AppConfig.TOPIC_NAME;

@Component
public class UserStreamingApp implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        //key
        Serde<String> stringSerde = Serdes.String();

        //Value
        Serde<User> userSerde = StreamsSerdes.UserSerde();

        StreamsBuilder streamsBuilder = new StreamsBuilder();

        //topology
        KStream<String, User> userKStream = streamsBuilder.stream(TOPIC_NAME, Consumed.with(stringSerde, userSerde));

       // KStream<String, User> adultsKStream = userKStream.filter((userid, user) -> user.getAge() > 18);
        userKStream
                .filter((id, user) -> user.getAge() > 25)
                .mapValues(User::getName)
                .to("names-topic", Produced.with(stringSerde, stringSerde));


        //adultsKStream.to("adults-topic", Produced.with(stringSerde, userSerde));


        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), getProperties());

        UserDatProducer.generateTestData();

        kafkaStreams.start();
        Thread.sleep(50_0000);
        kafkaStreams.close();
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        properties.put(StreamsConfig.CLIENT_ID_CONFIG , "my-awesome-client-app");
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "users-filter");
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "kafka-streams-app");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.BOOTSTRAP_SERVERS);
        return properties;
    }
}
